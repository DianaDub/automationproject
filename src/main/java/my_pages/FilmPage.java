package my_pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import utils.Urls;

public class FilmPage extends BasePage{
    public FilmPage(WebDriver driver) {
        super(driver);
    }
    private final static class Locators {
        private static final By authorization = By.xpath("//i[@class='fa fa-user']");
        private static final By loginName = By.id("login_name");
        private static final By loginPassword = By.id("login_password");
        private static final By enterButton = By.xpath("//button[@onclick='submit();']");
        private static final By searchButton = By.id("show-search");
        private static final By fieldForInsert = By.id("ajax_search");
        private static final By findButton = By.xpath("//button[@type='submit']");
        private static final By findingFilm = By.xpath("//div[@class='movie-img']");
        private static final By fieldForComment = By.xpath("//textarea[@id='comments']");
        private static final By buttonSendComment = By.xpath("//button[@name='submit']");
        private static final By deleteCommentButton = By.linkText("Видалити");
        private static final By buttonConfirmDeleting = By.xpath("(//span[@class='ui-button-text'])[2]");
        private static final By buttonOK = By.xpath("//span[@class='ui-button-text']");
        private static final By forLaterButton = By.xpath("(//li[@class=' mylisttooltip'])[1]");
        private static final By myLists = By.xpath("//a[@class='show-fav']");
        private static final By movieTitles = By.xpath("//a[@class='movie-title']");
        private static final By clickOnFilmInMyLists = By.linkText("Небезпечний елемент");
        private static final By addToMyListButton = By.xpath("//img[@src='/templates/uakino/images/dop-list.png']");
        private static final By myDefaultList = By.xpath("//span[@data-text='DefaultList']");
        private static final By myDefaultListPage = By.linkText("DefaultList");
        private static final By addNewListButton = By.xpath("//div[@onclick='createFavList();']/span");
        private static final By writeNameOfListField1 = By.name("fav-list-name");
        private static final By createList = By.xpath("//button[@onclick=\"saveFavList('create')\"]");
        private static final By deleteNewList = By.xpath("//span[@data-text='List for example']/parent::div/following-sibling::*");
        private static final By confirmDeletingList = By.xpath("(//span[@class='ui-button-text'])[2]");
        private static final By successDeletingMessage = By.id("dlepopup");
        private static final By deletingListOK = By.xpath("//span[@class='ui-button-text']");
        private static final By myCabinetButton = By.xpath("//span[@class='show-login']//span");
        private static final By exitButton = By.xpath("(//ul[@class='login-menu']/li)[6]");
        private static final By reviewedList = By.xpath("(//li[@class=' mylisttooltip'])[3]");
        private static final By reviewedListPage = By.linkText("Переглянуто");
        private static final By reviewedListBack = By.xpath("//li[@class=' active mylisttooltip']");
        private static final By laterListBack = By.xpath("//li[@class=' active mylisttooltip']");
    }
    public FilmPage openMainPage() {
        driver.get(Urls.uaKino);
        return this;
    }
    public FilmPage authorizationOpen() {
        workWithElements.click(FilmPage.Locators.authorization);
        return this;
    }
    public FilmPage insertWrightLoginName() {
        workWithElements.insertText(FilmPage.Locators.loginName, Strings.loginNameValue);
        return this;
    }
    public FilmPage insertWrightLoginPassword() {
        workWithElements.insertText(FilmPage.Locators.loginPassword, Strings.loginPasswordValue);
        return this;
    }
    public FilmPage enterButtonClick() {
        workWithElements.click(FilmPage.Locators.enterButton);
        return this;
    }
    public FilmPage searchButtonClick() {
        workWithElements.click(Locators.searchButton);
        return this;
    }
    public FilmPage insertInSearchField() {
        workWithElements.insertText(Locators.fieldForInsert, Strings.textForSearchField);
        return this;
    }
    public FilmPage findButtonClick() {
        workWithElements.click(Locators.findButton);
        return this;
    }
    public FilmPage findingFilmClick() {
        workWithElements.click(Locators.findingFilm);
        return this;
    }
    public FilmPage insertComment() {
        workWithElements.insertText(Locators.fieldForComment, Strings.textForCommentField);
        return this;
    }
    public FilmPage sendingComment() {
        workWithElements.click(Locators.buttonSendComment);
        return this;
    }
    public FilmPage buttonOKClick() {
        workWithElements.click(Locators.buttonOK);
        return this;
    }
    public FilmPage deleteComment() {
        workWithElements.click(Locators.deleteCommentButton);
        return this;
    }
    public FilmPage confirmDeletingComment() {
        workWithElements.click(Locators.buttonConfirmDeleting);
        return this;
    }
   public FilmPage addForLater() {
       workWithElements.click(Locators.forLaterButton);
       return this;
   }
    public FilmPage enterToMyLists() {
        workWithElements.click(Locators.myLists);
        return this;
    }
    public FilmPage checkFilmContainsInList(){
        System.out.println(workWithElements.returnTextFromElement(Locators.movieTitles).contains(Strings.textForSearchField));
        return this;
    }
    public FilmPage goToFilmPage() {
        workWithElements.click(Locators.clickOnFilmInMyLists);
        return this;
    }
    public FilmPage addToMyListButton(){
        workWithElements.click(Locators.addToMyListButton);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0, 500)");
        return this;
    }
    public FilmPage addToMyDefaultList() {
        workWithElements.click(Locators.myDefaultList);
        return this;
    }
    public FilmPage goToTheMyDefaultListPage() {
        workWithElements.click(Locators.myDefaultListPage);
        return this;
    }
    public FilmPage addNewListClick() {
        workWithElements.click(Locators.addNewListButton);
        return this;
    }
    public FilmPage insertNameOfList() {
        workWithElements.insertText(Locators.writeNameOfListField1, Strings.nameOfNewList);
        return this;
    }
    public FilmPage createListClick() {
        workWithElements.click(Locators.createList);
        return this;
    }
    public FilmPage deleteNewListClick() {
        workWithElements.click(Locators.deleteNewList);
        return this;
    }
    public FilmPage confirmDeleteListClick() {
        workWithElements.click(Locators.confirmDeletingList);
        return this;
    }
    public FilmPage checkMessageOfDeleteList(){
        System.out.println(workWithElements.returnTextFromElement(Locators.successDeletingMessage).contains(Strings.successMessageAboutDeletingOfList));
        return this;
    }
    public FilmPage deletingListOKButton() {
        workWithElements.click(Locators.deletingListOK);
        return this;
    }

    public FilmPage openCabinet() {
        workWithElements.click(FilmPage.Locators.myCabinetButton);
        return this;
    }
    public FilmPage exitFromCabinet() {
        workWithElements.click(FilmPage.Locators.exitButton);
        return this;
    }
    public FilmPage addToReviewedList() {
        workWithElements.click(Locators.reviewedList);
        return this;
    }
    public FilmPage openReviewedListPage() {
        workWithElements.click(Locators.reviewedListPage);
        return this;
    }
    public FilmPage deleteFromReviewedList() {
        workWithElements.click(Locators.reviewedListBack);
        return this;
    }
    public FilmPage deleteFromLaterList() {
        workWithElements.click(Locators.laterListBack);
        return this;
    }
    private static class Strings {
        private static final String loginNameValue = "Number11111";
        private static final String loginPasswordValue = "Number11111";
        private static final String textForSearchField = "Небезпечний елемент";
        private static final String textForCommentField = "Cool";
        private static final String nameOfNewList = "List for example";
        private static final String successMessageAboutDeletingOfList = "Вдало";
    }
}
