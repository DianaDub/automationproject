package my_pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import utils.Urls;

public class CorrectSelectionByCategoryPage extends BasePage{
    public CorrectSelectionByCategoryPage(WebDriver driver) {
        super(driver);
    }
    private final static class Locators {
        private static final By tabFilms = By.xpath("(//a[@href='/filmy/'])[1]");
        private static final By filmDetective = By.xpath("//a[@href='/filmy/genre_detective/']");
        private static final By anyFirstFilm = By.xpath("(//div[@class='movie-img'])[1]");
        private static final By genresOfFilm = By.xpath("//div[@itemprop='genre']");
        private static final By tabSeries = By.xpath("(//a[@href='/seriesss/'])[1]");
        private static final By seriesDoramy = By.xpath("//a[@href='/seriesss/doramy/']");
        private static final By anyFirstSeries = By.xpath("(//div[@class='movie-img'])[1]");
        private static final By genresOfSeries = By.xpath("(//div[@class='fi-desc'])[4]");
        private static final By tabCartoon = By.xpath("(//a[@href='/cartoon/'])[1]");
        private static final By seriesCartoon = By.xpath("//a[@href='/cartoonseries/']");
        private static final By anyFirstCartoon = By.xpath("(//div[@class='movie-img'])[1]");
        private static final By genresOfCartoon = By.xpath("(//div[@class='fi-item-s clearfix'])[4]");
        private static final By tab2024 = By.xpath("(//a[@href='/find/year/2024/'])[1]");
        private static final By bestFilms2021 = By.xpath("//a[@href='/filmy/best/2021/']");
        private static final By anyFirstFilm2021 = By.xpath("(//div[@class='movie-img'])[1]");
        private static final By yearOfFilm = By.xpath("(//div[@class='fi-desc'])[2]");
        private static final By seriesHBO = By.xpath("//a[@href='/colections/serialy-hbo/']");
        private static final By anySeriesHBO = By.xpath("(//div[@class='movie-img'])[3]");
        private static final By seriesOfHBO = By.xpath("//a[@class='colection-n-link']");
    }
    public CorrectSelectionByCategoryPage openMainPage() {
        driver.get(Urls.uaKino);
        return this;
    }
    public CorrectSelectionByCategoryPage moveToFilms() {
        Actions actions = new Actions(driver);
        actions.moveToElement(driver.findElement(Locators.tabFilms)).perform();
        return this;
    }
    public CorrectSelectionByCategoryPage openFilmsPage() {
        workWithElements.click(Locators.filmDetective);
        return this;
    }
    public CorrectSelectionByCategoryPage openAnyFirstFilm() {
        workWithElements.click(Locators.anyFirstFilm);
        return this;
    }
    public CorrectSelectionByCategoryPage genresFilmContain() {
        System.out.println(workWithElements.returnTextFromElement(Locators.genresOfFilm).contains("Детективи"));
        return this;
    }
    public CorrectSelectionByCategoryPage moveToSeries() {
        Actions actions = new Actions(driver);
        actions.moveToElement(driver.findElement(Locators.tabSeries)).perform();
        return this;
    }
    public CorrectSelectionByCategoryPage openSeriesPage() {
        workWithElements.click(Locators.seriesDoramy);
        return this;
    }
    public CorrectSelectionByCategoryPage openAnyFirstSeries() {
        workWithElements.click(Locators.anyFirstSeries);
        return this;
    }
    public CorrectSelectionByCategoryPage genresSerieContain() {
        System.out.println(workWithElements.returnTextFromElement(Locators.genresOfSeries).contains("Дорами"));
        return this;
    }
    public CorrectSelectionByCategoryPage moveToCartoons() {
        Actions actions = new Actions(driver);
        actions.moveToElement(driver.findElement(Locators.tabCartoon)).perform();
        return this;
    }
    public CorrectSelectionByCategoryPage openCartoonsPage() {
        workWithElements.click(Locators.seriesCartoon);
        return this;
    }
    public CorrectSelectionByCategoryPage openAnyFirstCartoon() {
        workWithElements.click(Locators.anyFirstCartoon);
        return this;
    }
    public CorrectSelectionByCategoryPage genresCartoonContain() {
        System.out.println(workWithElements.returnTextFromElement(Locators.genresOfCartoon).contains("Мультсеріали"));
        return this;
    }
    public CorrectSelectionByCategoryPage moveTo2024() {
        Actions actions = new Actions(driver);
        actions.moveToElement(driver.findElement(Locators.tab2024)).perform();
        return this;
    }
    public CorrectSelectionByCategoryPage openBest2021FilmsPage() {
        workWithElements.click(Locators.bestFilms2021);
        return this;
    }
    public CorrectSelectionByCategoryPage openAnyFirstFilm2021() {
        workWithElements.click(Locators.anyFirstFilm2021);
        return this;
    }
    public CorrectSelectionByCategoryPage yearFilmContain() {
        System.out.println(workWithElements.returnTextFromElement(Locators.yearOfFilm).contains("2021"));
        return this;
    }
    public CorrectSelectionByCategoryPage openSeriesHBO() {
        workWithElements.click(Locators.seriesHBO);
        return this;
    }
    public CorrectSelectionByCategoryPage openAnySeriesHBO() {
        workWithElements.click(Locators.anySeriesHBO);
        return this;
    }
    public CorrectSelectionByCategoryPage hboSeriesContain() {
        System.out.println(workWithElements.returnTextFromElement(Locators.seriesOfHBO).contains("HBO"));
        return this;
    }
}
