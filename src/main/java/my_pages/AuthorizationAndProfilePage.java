package my_pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import utils.Urls;

public class AuthorizationAndProfilePage extends BasePage{
    public AuthorizationAndProfilePage(WebDriver driver) {
        super(driver);
    }
    private final static class Locators {
        private static final By authorization = By.xpath("//i[@class='fa fa-user']");
        private static final By register = By.xpath("//a[@class='log-register']");
        private static final By registerName = By.id("name");
        private static final By registerPasswordField = By.id("password1");
        private static final By registerPasswordField2 = By.id("password2");
        private static final By registerMail = By.id("email");
        private static final By registerSendButton = By.xpath("//button[@name='submit']");
        private static final By registerFullname = By.id("fullname");
        private static final By myCabinetButton = By.xpath("//span[@class='show-login']//span");
        private static final By exitButton = By.xpath("(//ul[@class='login-menu']/li)[6]");
        private static final By loginName = By.id("login_name");
        private static final By loginPassword = By.id("login_password");
        private static final By enterButton = By.xpath("//button[@onclick='submit();']");
        private static final By errorMessage = By.xpath("//div[@class='berrors']/b");
        private static final By customerNameInMyCabinet = By.className("login-title");
        private static final By myProfile = By.xpath("(//ul[@class='login-menu']/li)[1]");
        private static final By editProfileButton = By.xpath("(//div[@class='usp__btn']/a)[2]");
        private static final By aboutYourselfField = By.xpath("(//div[@class='form__content']/textarea)[2]");
        private static final By updateButton = By.xpath("//button[@name='submit']");
        private static final By writeMessageButton = By.xpath("(//div[@class='usp__btn'])[1]");
        private static final By writeMessageField1 = By.id("pm_subj");
        private static final By writeMessageField2 = By.id("pm_text");
        private static final By sendMessageButton = By.xpath("(//button[@type='button'])[2]");
        private static final By successMessage = By.id("dlepopup");
        private static final By okButton = By.xpath("//button[@type='button']");
    }
    public AuthorizationAndProfilePage openMainPage() {
        driver.get(Urls.uaKino);
        return this;
    }
    public AuthorizationAndProfilePage authorizationOpen() {
        workWithElements.click(AuthorizationAndProfilePage.Locators.authorization);
        return this;
    }
    public AuthorizationAndProfilePage openRegisterPage() {
        workWithElements.click(AuthorizationAndProfilePage.Locators.register);
        return this;
    }
    public AuthorizationAndProfilePage insertName() {
        workWithElements.insertText(AuthorizationAndProfilePage.Locators.registerName, AuthorizationAndProfilePage.Strings.registerNameValue);
        return this;
    }
    public AuthorizationAndProfilePage insertPassword() {
        workWithElements.insertText(AuthorizationAndProfilePage.Locators.registerPasswordField, AuthorizationAndProfilePage.Strings.registerPasswordValue);
        return this;
    }
    public AuthorizationAndProfilePage insertPassword2() {
        workWithElements.insertText(AuthorizationAndProfilePage.Locators.registerPasswordField2, AuthorizationAndProfilePage.Strings.registerPasswordValue);
        return this;
    }
    public AuthorizationAndProfilePage insertMail() {
        workWithElements.insertText(AuthorizationAndProfilePage.Locators.registerMail, AuthorizationAndProfilePage.Strings.registerMailValue);
        return this;
    }
    public AuthorizationAndProfilePage sendRegisterForm() {
        workWithElements.click(AuthorizationAndProfilePage.Locators.registerSendButton);
        return this;
    }
    public AuthorizationAndProfilePage insertFullName() {
        workWithElements.insertText(AuthorizationAndProfilePage.Locators.registerFullname, AuthorizationAndProfilePage.Strings.registerFullnameValue);
        return this;
    }
    public AuthorizationAndProfilePage openCabinet() {
        workWithElements.click(AuthorizationAndProfilePage.Locators.myCabinetButton);
        return this;
    }
    public AuthorizationAndProfilePage exitFromCabinet() {
        workWithElements.click(AuthorizationAndProfilePage.Locators.exitButton);
        return this;
    }
    public AuthorizationAndProfilePage openMyProfile() {
        workWithElements.click(Locators.myProfile);
        return this;
    }
    public AuthorizationAndProfilePage insertLoginName() {
        workWithElements.insertText(Locators.loginName, Strings.loginNameValue);
        return this;
    }
    public AuthorizationAndProfilePage insertLoginPassword() {
        workWithElements.insertText(Locators.loginPassword, Strings.loginPasswordValue);
        return this;
    }
    public AuthorizationAndProfilePage enterButtonClick() {
        workWithElements.click(Locators.enterButton);
        return this;
    }
    public AuthorizationAndProfilePage assertErrorMessage() {
        Assert.assertEquals(workWithElements.returnTextFromElement(Locators.errorMessage), Strings.errorAuthorizationMessage);
        return this;
    }
    public AuthorizationAndProfilePage insertWrightLoginName() {
        workWithElements.insertText(Locators.loginName, Strings.registerNameValue);
        return this;
    }
    public AuthorizationAndProfilePage insertWrightLoginPassword() {
        workWithElements.insertText(Locators.loginPassword, Strings.registerPasswordValue);
        return this;
    }
    public AuthorizationAndProfilePage assertCustomerName() {
        Assert.assertEquals(workWithElements.returnTextFromElement(Locators.customerNameInMyCabinet), Strings.registerNameValue);
        return this;
    }
    public AuthorizationAndProfilePage openEditMyProfile() {
        workWithElements.click(Locators.editProfileButton);
        return this;
    }
    public AuthorizationAndProfilePage insertTextInFieldAboutYourself() {
        workWithElements.insertText(Locators.aboutYourselfField, Strings.textForFieldAboutYourself);
        return this;
    }
    public AuthorizationAndProfilePage tapUpdateButton() {
        workWithElements.click(Locators.updateButton);
        return this;
    }
    public AuthorizationAndProfilePage tapWritingMessageButton() {
        workWithElements.click(Locators.writeMessageButton);
        return this;
    }
    public AuthorizationAndProfilePage insertMessageField1() {
        workWithElements.insertText(Locators.writeMessageField1, Strings.writeMessageText1);
        return this;
    }
    public AuthorizationAndProfilePage insertMessageField2() {
        workWithElements.insertText(Locators.writeMessageField2, Strings.writeMessageText2);
        return this;
    }
    public AuthorizationAndProfilePage sendMessageButton() {
        workWithElements.click(Locators.sendMessageButton);
        return this;
    }
    public AuthorizationAndProfilePage assertSuccessMessageAboutMessage() {
        Assert.assertEquals(workWithElements.returnTextFromElement(Locators.successMessage), Strings.successMessageAboutSendingMessage);
        return this;
    }
    public AuthorizationAndProfilePage tapOKButton() {
        workWithElements.click(Locators.okButton);
        return this;
    }
    private static class Strings {
        private static final String registerNameValue = "example1039";
        private static final String registerPasswordValue = "password1234567890";
        private static final String registerMailValue = "example1039@example.com";
        private static final String registerFullnameValue = "Example";
        private static final String loginNameValue = "example098";
        private static final String loginPasswordValue = "password";
        private static final String errorAuthorizationMessage = "Помилка авторизації";
        private static final String textForFieldAboutYourself = "I like movies.";
       private static final String writeMessageText1 = "Hello!";
       private static final String writeMessageText2 = "How are you?";
       private static final String successMessageAboutSendingMessage = "Ваше повідомлення успішно відправлене.";
    }
}
