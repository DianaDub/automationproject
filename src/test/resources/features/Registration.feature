Feature: Testing of Registration Page
  Background:
    Given set up driver
    @registration
  Scenario: Test registration of customer
    Given User is on page of the site
    When Go to the page of registration
    And Insert valid data in the necessary fields and send form
    And Insert full name of the customer and send form
    And Exit from the page in tab My cabinet
    Then The customer's page is closed