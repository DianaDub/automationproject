package my_tests;

import io.qameta.allure.*;
import org.testng.annotations.Test;

public class AuthorizationAndProfileTest extends BaseTest{
    @Test
    @Description("This is check of registration.")
    @Feature("NORMAL severity test")
    //@TmsLink("https://didub2002.testrail.io/index.php?/tests/view/104&group_by=cases:section_id&group_order=asc&group_id=26")
    @Severity(SeverityLevel.NORMAL)
    public void checkRegistration() {
        firstPage.openMainPage()
                .authorizationOpen()
                .openRegisterPage()
                .insertName()
                .insertPassword()
                .insertPassword2()
                .insertMail()
                .sendRegisterForm()
                .insertFullName()
                .sendRegisterForm()
                .openCabinet()
                .exitFromCabinet();
    }
    @Test
    @Description("This is check of authorization with wrong data.")
    @Feature("MINOR severity test")
    //@TmsLink("https://didub2002.testrail.io/index.php?/tests/view/104&group_by=cases:section_id&group_order=asc&group_id=26")
    @Severity(SeverityLevel.MINOR)
    public void enterWithWrongData() {
        firstPage.openMainPage()
                .authorizationOpen()
                .insertLoginName()
                .insertLoginPassword()
                .enterButtonClick()
                .assertErrorMessage();
    }
    @Test
    @Description("This is check of authorization with wright data.")
    @Feature("NORMAL severity test")
    //@TmsLink("https://didub2002.testrail.io/index.php?/tests/view/104&group_by=cases:section_id&group_order=asc&group_id=26")
    @Severity(SeverityLevel.NORMAL)
    public void enterWithWrightData() {
        firstPage.openMainPage()
                .authorizationOpen()
                .insertWrightLoginName()
                .insertWrightLoginPassword()
                .enterButtonClick()
                .openCabinet()
                .assertCustomerName()
                .exitFromCabinet();
    }
    @Test
    @Description("This is check of edition of the information about customer.")
    @Feature("MINOR severity test")
    //@TmsLink("https://didub2002.testrail.io/index.php?/tests/view/104&group_by=cases:section_id&group_order=asc&group_id=26")
    @Severity(SeverityLevel.MINOR)
    public void editInformationAboutCustomer() {
        firstPage.openMainPage()
                .authorizationOpen()
                .insertWrightLoginName()
                .insertWrightLoginPassword()
                .enterButtonClick()
                .openCabinet()
                .openMyProfile()
                .openEditMyProfile()
                .insertTextInFieldAboutYourself()
                .tapUpdateButton()
                .openCabinet()
                .exitFromCabinet();
    }
    @Test
    @Description("This is check of sending message to a customer.")
    @Feature("MINOR severity test")
    //@TmsLink("https://didub2002.testrail.io/index.php?/tests/view/104&group_by=cases:section_id&group_order=asc&group_id=26")
    @Severity(SeverityLevel.MINOR)
    public void sendingMessageToCustomer() {
        firstPage.openMainPage()
                .authorizationOpen()
                .insertWrightLoginName()
                .insertWrightLoginPassword()
                .enterButtonClick()
                .openCabinet()
                .openMyProfile()
                .tapWritingMessageButton()
                .insertMessageField1()
                .insertMessageField2()
                .sendMessageButton()
                .assertSuccessMessageAboutMessage()
                .tapOKButton()
                .openCabinet()
                .exitFromCabinet();
    }
}