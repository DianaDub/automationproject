package my_tests;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Test;

public class FilmTest extends BaseTest{
    @Test
    @Description("This is check of adding and deleting comments.")
    @Feature("MINOR severity test")
    //@TmsLink("https://didub2002.testrail.io/index.php?/tests/view/104&group_by=cases:section_id&group_order=asc&group_id=26")
    @Severity(SeverityLevel.MINOR)
    public void checkAddAndDeletingComment() {
        thirdPage.openMainPage()
                .authorizationOpen()
                .insertWrightLoginName()
                .insertWrightLoginPassword()
                .enterButtonClick()
                .searchButtonClick()
                .insertInSearchField()
                .findButtonClick()
                .findingFilmClick()
                .insertComment()
                .sendingComment()
                .buttonOKClick()
                .sendingComment()
                .deleteComment()
                .confirmDeletingComment()
                .openCabinet()
                .exitFromCabinet();
    }
    @Test
    @Description("This is check of adding and deleting movie from LaterList.")
    @Feature("NORMAL severity test")
    //@TmsLink("https://didub2002.testrail.io/index.php?/tests/view/104&group_by=cases:section_id&group_order=asc&group_id=26")
    @Severity(SeverityLevel.NORMAL)
    public void checkMyLaterList(){
        thirdPage.openMainPage()
                .authorizationOpen()
                .insertWrightLoginName()
                .insertWrightLoginPassword()
                .enterButtonClick()
                .searchButtonClick()
                .insertInSearchField()
                .findButtonClick()
                .findingFilmClick()
                .addForLater()
                .enterToMyLists()
                .checkFilmContainsInList()
                .goToFilmPage()
                .deleteFromLaterList()
                .openCabinet()
                .exitFromCabinet();
    }
    @Test
    @Description("This is check of adding and deleting movie from MyDefaultList.")
    @Feature("MINOR severity test")
    //@TmsLink("https://didub2002.testrail.io/index.php?/tests/view/104&group_by=cases:section_id&group_order=asc&group_id=26")
    @Severity(SeverityLevel.MINOR)
    public void checkAddToDefaultList(){
        thirdPage.openMainPage()
                .authorizationOpen()
                .insertWrightLoginName()
                .insertWrightLoginPassword()
                .enterButtonClick()
                .searchButtonClick()
                .insertInSearchField()
                .findButtonClick()
                .findingFilmClick()
                .addToMyListButton()
                .addToMyDefaultList()
                .enterToMyLists()
                .goToTheMyDefaultListPage()
                .checkFilmContainsInList()
                .goToFilmPage()
                .addToMyListButton()
                .addToMyDefaultList()
                .openCabinet()
                .exitFromCabinet();
    }
    @Test
    @Description("This is check of adding and deleting new list.")
    @Feature("NORMAL severity test")
    //@TmsLink("https://didub2002.testrail.io/index.php?/tests/view/104&group_by=cases:section_id&group_order=asc&group_id=26")
    @Severity(SeverityLevel.NORMAL)
    public void checkAddAndDeleteNewList(){
        thirdPage.openMainPage()
                .authorizationOpen()
                .insertWrightLoginName()
                .insertWrightLoginPassword()
                .enterButtonClick()
                .searchButtonClick()
                .insertInSearchField()
                .findButtonClick()
                .findingFilmClick()
                .addToMyListButton()
                .addNewListClick()
                .insertNameOfList()
                .createListClick()
                .deleteNewListClick()
                .confirmDeleteListClick()
                .checkMessageOfDeleteList()
                .deletingListOKButton()
                .openCabinet()
                .exitFromCabinet();
    }
    @Test
    @Description("This is check of adding and deleting movie from ReviewedList.")
    @Feature("TRIVIAL severity test")
    //@TmsLink("https://didub2002.testrail.io/index.php?/tests/view/104&group_by=cases:section_id&group_order=asc&group_id=26")
    @Severity(SeverityLevel.TRIVIAL)
    public void checkAddToReviewedList(){
        thirdPage.openMainPage()
                .authorizationOpen()
                .insertWrightLoginName()
                .insertWrightLoginPassword()
                .enterButtonClick()
                .searchButtonClick()
                .insertInSearchField()
                .findButtonClick()
                .findingFilmClick()
                .addToReviewedList()
                .enterToMyLists()
                .openReviewedListPage()
                .checkFilmContainsInList()
                .goToFilmPage()
                .deleteFromReviewedList()
                .openCabinet()
                .exitFromCabinet();
    }
}
