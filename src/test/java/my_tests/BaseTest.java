package my_tests;

import driver_init.DriverInit;
import functions.CustomWaiters;
import functions.WorkWithElements;
import my_pages.BasePage;
import my_pages.AuthorizationAndProfilePage;
import my_pages.CorrectSelectionByCategoryPage;
import my_pages.FilmPage;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Listeners;
import utils.MyAllureTestListeners;

@Listeners({MyAllureTestListeners.class})
public class BaseTest {
    protected WebDriver driver = DriverInit.startDriver();;
    protected CustomWaiters waiters = new CustomWaiters(driver);
    protected WorkWithElements workWithElements = new WorkWithElements(driver, waiters);
    protected BasePage basePage = new BasePage(driver);
    protected AuthorizationAndProfilePage firstPage = new AuthorizationAndProfilePage(driver);
    protected CorrectSelectionByCategoryPage secondPage = new CorrectSelectionByCategoryPage(driver);
    protected FilmPage thirdPage = new FilmPage(driver);

    @BeforeSuite
    public void init(){
        driver = DriverInit.startDriver();
    }
    @AfterSuite
    public void stop() {
        driver.quit();
    }
}
