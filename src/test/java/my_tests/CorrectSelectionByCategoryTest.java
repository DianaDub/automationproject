package my_tests;

import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Test;

public class CorrectSelectionByCategoryTest extends BaseTest{
    @Test
    @Feature("NORMAL severity test")
    //@TmsLink("https://didub2002.testrail.io/index.php?/tests/view/104&group_by=cases:section_id&group_order=asc&group_id=26")
    @Severity(SeverityLevel.NORMAL)
    public void checkGenreFilm() {
        secondPage.openMainPage()
                .moveToFilms()
                .openFilmsPage()
                .openAnyFirstFilm()
                .genresFilmContain();
    }
    @Test
    @Feature("MINOR severity test")
    //@TmsLink("https://didub2002.testrail.io/index.php?/tests/view/104&group_by=cases:section_id&group_order=asc&group_id=26")
    @Severity(SeverityLevel.MINOR)
    public void checkGenreSeries() {
        secondPage.openMainPage()
                .moveToSeries()
                .openSeriesPage()
                .openAnyFirstSeries()
                .genresSerieContain();
    }
    @Test
    @Feature("MINOR severity test")
    //@TmsLink("https://didub2002.testrail.io/index.php?/tests/view/104&group_by=cases:section_id&group_order=asc&group_id=26")
    @Severity(SeverityLevel.MINOR)
    public void checkGenreCartoons() {
        secondPage.openMainPage()
                .moveToCartoons()
                .openCartoonsPage()
                .openAnyFirstCartoon()
                .genresCartoonContain();
    }
    @Test
    @Feature("NORMAL severity test")
    //@TmsLink("https://didub2002.testrail.io/index.php?/tests/view/104&group_by=cases:section_id&group_order=asc&group_id=26")
    @Severity(SeverityLevel.NORMAL)
    public void checkFilmsYear() {
        secondPage.openMainPage()
                .moveTo2024()
                .openBest2021FilmsPage()
                .openAnyFirstFilm2021()
                .yearFilmContain();
    }
    @Test
    @Feature("MINOR severity test")
    //@TmsLink("https://didub2002.testrail.io/index.php?/tests/view/104&group_by=cases:section_id&group_order=asc&group_id=26")
    @Severity(SeverityLevel.MINOR)
    public void checkSeriesHBO() {
        secondPage.openMainPage()
                .moveToSeries()
                .openSeriesHBO()
                .openAnySeriesHBO()
                .hboSeriesContain();
    }
}
